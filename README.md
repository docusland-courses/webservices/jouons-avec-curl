# Jouons avec cURL

Prise en main de curl et compréhension des headers

## Exercices : 
L'objectif de cet exercice est d'utiliser curl afin de voir le principe des entêtes HTTP. 
Tout d'abord qu'est-que cURL? Il s'agit d'un client en ligne de commande pour appeler une URL et en récupérer la reponse.

Avant de pouvoir réaliser cet exercice, nous allons il est nécessaire de comprendre quelques paramètres de curl :
- `-i`: Afficher les entêtes de la réponse
- `-I` : Afficher uniquement les entêtes de la réponse
- `-H` : Ajouter un/plusieurs entête lors de l'appel

Executer les requêtes suivantes pour mieux comprendre les headers : 


### Exercice 1
Récupérer le contenu de l'url suivante : `https://examplecat.com/cat.txt`

### Exercice 2
Extraire une sous partie de la réponse en lisant uniquement les bytes [8, 17]. Uniquement ces données doivent être retournées par le serveur

### Exercice 3
Récupérer le contenu de l'url suivante, `https://examplecat.com/` en format compressé **gzip**

### Exercice 4
Récupérer le contenu du site `https://www.twilio.com`  en langue espagnole

### Exercice 5
L'URL a changé, récupérer les routes par lesquelles passe une transaction vers le site `http://httpstatuses.com`

### Exercice 6
Intéroger l'URL `https://examplecat.com/bananas` afin de voir comment est gérée une erreur 404.

### Exercice 7
Le dernier Etag fourni par mon CDN de jQuery `https://code.jquery.com/jquery-3.6.0.min.js` était `611feac9-15d9d`,réaliser une requête permetant de savoir si une nouvelle version est disponible. 
